package edu.washington.jamys2.geopost;

import android.annotation.TargetApi;
import android.app.Activity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.HashMap;
import helper.SQLiteHandler;

public class SearchUsers extends Activity {

    @TargetApi(Build.VERSION_CODES.KITKAT)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_users);

        final ListView listView = (ListView) findViewById(R.id.listView);
        String[] topics = new String[] {"John Snickerdoodle", "Lyonel Fruits", "Hiro Nakamura", "Jamy Southafeng", "Thunder Cats", "test"};

        ArrayAdapter<String> adapter = new ArrayAdapter<String> (this, android.R.layout.simple_list_item_1, android.R.id.text1, topics);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String itemValue = (String)listView.getItemAtPosition(position);
                Toast.makeText(getApplicationContext(), "You have selected " + itemValue, Toast.LENGTH_SHORT).show();
                Intent nextActivity = new Intent(SearchUsers.this, Profile.class);
                nextActivity.putExtra("topics", itemValue);
                startActivity(nextActivity);
            }
        });

        /*
        ListView list = (ListView) findViewById(R.id.listView);

        // SqLite database handler
        SQLiteHandler db = new SQLiteHandler(getApplicationContext());


        // Fetching user details from sqlite
        HashMap<String, String> user = db.getUserDetails();

        ArrayList<String> array = new ArrayList<>();
        JSONArray json = null;
        try {
            json = new JSONArray(user);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        for (int i =0; i< json.length(); i++) {
            try {
                array.add(json.getString(i));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, android.R.id.text1, array);
        list.setAdapter(adapter);
        */
    }

}