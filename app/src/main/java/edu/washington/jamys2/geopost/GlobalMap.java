package edu.washington.jamys2.geopost;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.ParcelFileDescriptor;
import android.provider.MediaStore;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.FileDescriptor;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by Anyone on 3/15/2015.
 */
public class GlobalMap extends FragmentActivity{
    private GoogleMap mMap; // Might be null if Google Play services APK is not available.
    private static final int SELECT_PICTURE = 1;
    private Bitmap bitmap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.global_map);

        setUpMapIfNeeded();
        mMap.setMyLocationEnabled(true);

        ArrayList<LatLng> pointList = new ArrayList<LatLng>();
        pointList.add(new LatLng(47.655189,-122.308036));
        pointList.add(new LatLng(47.655335,-122.30352));
        pointList.add(new LatLng(47.657704,-122.317649));
        pointList.add(new LatLng(47.661273,-122.30971));
        pointList.add(new LatLng(47.662631,-122.292303));
        pointList.add(new LatLng(47.673324,-122.293762));

        ArrayList<Integer> pictures = new ArrayList<Integer>();
        pictures.add(R.drawable.one);
        pictures.add(R.drawable.two);
        pictures.add(R.drawable.three);
        pictures.add(R.drawable.four);
        pictures.add(R.drawable.five);
        pictures.add(R.drawable.six);


        for(int i=0;i<pointList.size();i++){
            drawMarker(pointList.get(i), pictures.get(i));
        }

        // Set a custom info window adapter for the google map
        mMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {

            // Use default InfoWindow frame
            @Override
            public View getInfoWindow(Marker arg0) {
                return null;
            }

            // Defines the contents of the InfoWindow
            // Button is not actually clickable, it's to visually let users know to remove marker
            @Override
            public View getInfoContents(Marker marker) {
                View v = getLayoutInflater().inflate(R.layout.window, null);
                LatLng latLng = marker.getPosition();
                TextView lat = (TextView) v.findViewById(R.id.tv_lat);
                TextView lng = (TextView) v.findViewById(R.id.tv_lng);
                ImageView imageView = (ImageView) v.findViewById(R.id.tv_image);
                //Button btnRemove = (Button) v.findViewById(R.id.btnRemove);
                lat.setText("Latitude:" + latLng.latitude);
                lng.setText("Longitude:" + latLng.longitude);
                //btnRemove.setText("Remove Marker");
                if (bitmap != null) {
                    android.view.ViewGroup.LayoutParams layoutParams = imageView.getLayoutParams();
                    layoutParams.width = 220;
                    layoutParams.height = 220;
                    imageView.setLayoutParams(layoutParams);
                    imageView.setImageBitmap(bitmap);
                }

                // Returning the view containing InfoWindow contents
                return v;

            }
        });

        // Add marker on long click; show marker on click
        mMap.setOnMapLongClickListener(new GoogleMap.OnMapLongClickListener() {
            @Override
            public void onMapLongClick(LatLng position) {
                bitmap = null;
                MarkerOptions markerOptions = new MarkerOptions().draggable(true);
                markerOptions.position(position);
                mMap.animateCamera(CameraUpdateFactory.newLatLng(position));
                Marker marker = mMap.addMarker(markerOptions);
                marker.showInfoWindow();
                mMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
                    @Override
                    public void onInfoWindowClick(Marker marker) {
                        //marker.remove();
                        Intent intent = new Intent();
                        intent.setType("image/*");
                        intent.setAction(Intent.ACTION_GET_CONTENT);
                        startActivityForResult(Intent.createChooser(intent,
                                "Select Picture"), SELECT_PICTURE);
                    }
                });
                mMap.setOnMarkerDragListener(new GoogleMap.OnMarkerDragListener() {
                    @Override
                    public void onMarkerDragStart(Marker marker) {
                    }

                    @Override
                    public void onMarkerDrag(Marker marker) {
                    }

                    @Override
                    public void onMarkerDragEnd(Marker marker) {
                        View v = getLayoutInflater().inflate(R.layout.window, null);
                        LatLng latLng = marker.getPosition();
                        TextView lat = (TextView) v.findViewById(R.id.tv_lat);
                        TextView lng = (TextView) v.findViewById(R.id.tv_lng);
                        lat.setText("Latitude:" + latLng.latitude);
                        lng.setText("Longitude:" + latLng.longitude);
                    }
                });
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        setUpMapIfNeeded();
    }

    /**
     * Sets up the map if it is possible to do so (i.e., the Google Play services APK is correctly
     * installed) and the map has not already been instantiated.. This will ensure that we only ever
     * call {@link #setUpMap()} once when {@link #mMap} is not null.
     * <p/>
     * If it isn't installed {@link com.google.android.gms.maps.SupportMapFragment} (and
     * {@link com.google.android.gms.maps.MapView MapView}) will show a prompt for the user to
     * install/update the Google Play services APK on their device.
     * <p/>
     * A user can return to this FragmentActivity after following the prompt and correctly
     * installing/updating/enabling the Google Play services. Since the FragmentActivity may not
     * have been completely destroyed during this process (it is likely that it would only be
     * stopped or paused), {@link #onCreate(Bundle)} may not be called again so we should call this
     * method in {@link #onResume()} to guarantee that it will be called.
     */
    private void setUpMapIfNeeded() {
        // Do a null check to confirm that we have not already instantiated the map.
        if (mMap == null) {
            // Try to obtain the map from the SupportMapFragment.
            mMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map))
                    .getMap();
            // Check if we were successful in obtaining the map.
            if (mMap != null) {
                setUpMap();
            }
        }
    }

    /**
     * This is where we can add markers or lines, add listeners or move the camera. In this case, we
     * just add a marker near Africa.
     * <p/>
     * This should only be called once and when we are sure that {@link #mMap} is not null.
     */
    private void setUpMap() {
        // Default position
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(47.6553351, -122.3035199), 14.0f));
    }


    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            if (requestCode == SELECT_PICTURE) {
                Uri selectedImageUri = data.getData();
                if (Build.VERSION.SDK_INT < 19) {
                    String selectedImagePath = getPath(selectedImageUri);
                    bitmap = BitmapFactory.decodeFile(selectedImagePath);
                }
                else {
                    ParcelFileDescriptor parcelFileDescriptor;
                    try {
                        parcelFileDescriptor = getContentResolver().openFileDescriptor(selectedImageUri, "r");
                        FileDescriptor fileDescriptor = parcelFileDescriptor.getFileDescriptor();
                        bitmap = BitmapFactory.decodeFileDescriptor(fileDescriptor);
                        parcelFileDescriptor.close();

                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    /**
     * helper to retrieve the path of an image URI
     */
    public String getPath(Uri uri) {
        if( uri == null ) {
            return null;
        }
        String[] projection = { MediaStore.Images.Media.DATA };
        Cursor cursor = getContentResolver().query(uri, projection, null, null, null);
        if( cursor != null ){
            int column_index = cursor
                    .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        }
        return uri.getPath();
    }

    // Draw markers
    private void drawMarker(LatLng point, int pictures){
        View v = getLayoutInflater().inflate(R.layout.window, null);
        TextView lat = (TextView) v.findViewById(R.id.tv_lat);
        TextView lng = (TextView) v.findViewById(R.id.tv_lng);
        ImageView imageView = (ImageView) v.findViewById(R.id.tv_image);
        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(point);
        lat.setText("Latitude:" + point.latitude);
        lng.setText("Longitude:" + point.longitude);
        android.view.ViewGroup.LayoutParams layoutParams = imageView.getLayoutParams();
        layoutParams.width = 220;
        layoutParams.height = 220;
        imageView.setLayoutParams(layoutParams);
        imageView.setImageResource(pictures);
        mMap.addMarker(markerOptions);
    }
}
