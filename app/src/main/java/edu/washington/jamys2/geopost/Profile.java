package edu.washington.jamys2.geopost;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

/**
 * Created by Anyone on 3/15/2015.
 */
public class Profile extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Intent launchedMe = getIntent();
        String itemValue = launchedMe.getStringExtra("topics");
        TextView txtName = (TextView) findViewById(R.id.name);
        TextView txtEmail = (TextView) findViewById(R.id.email);
        Button btnHome = (Button) findViewById(R.id.btnHome);
        Button btnLogout = (Button) findViewById(R.id.btnLogout);

        if (itemValue.equals("John Snickerdoodle")) {
            txtName.setText(itemValue);
            txtEmail.setText("JohnS001@businessnow.com");
        } else if (itemValue.equals("Lyonel Fruits")) {
            txtName.setText(itemValue);
            txtEmail.setText("FruitWarrior2112@hotmail.com");
        } else if (itemValue.equals("Hiro Nakamura")) {
            txtName.setText(itemValue);
            txtEmail.setText("NiponHiroNaka@jp.jp");
        } else if (itemValue.equals("Jamy Southafeng")) {
            txtName.setText(itemValue);
            txtEmail.setText("jamys2@uw.edu");
        } else if (itemValue.equals("Thunder Cats")) {
            txtName.setText("Thunder Cats");
            txtEmail.setText("ThunderCats@gmail.com");
        } else {
            txtName.setText("test");
            txtEmail.setText("test@test.com");
        }
        btnHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Profile.this, Home.class);
                startActivity(intent);
            }
        });

        // Logout button click event
        btnLogout.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                logoutUser();
            }
        });
    }

    /**
     * Logging out the user. Will set isLoggedIn flag to false in shared
     * preferences Clears the user data from sqlite users table
     * */
    private void logoutUser() {

        // Launching the login activity
        Intent intent = new Intent(Profile.this, LoginActivity.class);
        startActivity(intent);
        finish();
    }
}
