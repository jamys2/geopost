Final Project

Find a new type of project someone has not come up with and implement them. The goal is functionality, not too much focus on UI/looks. 

My project is called GeoPost. The goal of this project is to create an picture diary service, such as how Yelp does food services. For any location a user can upload a marker at a location, upload a photo, and implement that to their "diary map". This diary map is only accessible to themselves and friends. A global map is where users can choose to upload their marker to a map shared by all. These markers can cannot be edited by other users, but can be observed.

Due to time and other schedule constraints, I could not add as much functionality as I would have liked. Given more time, this project could look much better!

Video in action:
https://youtu.be/b5x9uGyGf4Q

Below are functions that work in the app right now.

* Login/Registration:
* - Checks valid information
* - Creates an account to remote server/database
* 
* Profile Page:
* - Show name, email, log out button
* - If logged out, sent back to log in page
* 
* Camera:
* - User can take picture and added to SD card
* - Can verify picture through preview
*  
* User Map:
* - Default location around UW
* - Can utilize Google Maps API (directions, geo-locate user, etc)
* - User can add markers by long click/hold 
* - Initial markers detail latitude and longitude
* - Markers are can be dragged and moved. Lat/Long will auto adjust.
* - User can press on marker for window to pop. User can press the window to add photos.
* - Photos are added to the marker's window
* 
* Global Map:
* - Same as User Map, except there are markers already implemented to show global use.
* 
* Search Users:
* - List of users are displayed
* - Upon clicking a user, it leads to the profile page